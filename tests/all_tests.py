import base_test


class LoginTestCases(base_test.BaseTest):
    def test_01_test_login(self):
        # Step 1:Check we're on the Welcome Page
        assert self.welcomepage.check_page()
        assert self.welcomepage.click_login(self.loginpage)
        #Step 2: Login to the webpage and confirm the main page appeears
        assert self.loginpage.login(self.mainpage,self.param.username,self.param.password)
        #Step 3: Logout
        assert self.mainpage.click_logout(self.welcomepage)

